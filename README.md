# useful links

## Hacking the Kobo Clara HD:

- [1: Hacking the internal microSD storage](https://yingtongli.me/blog/2018/07/29/kobo-sd.html)
- [2: Bypassing registration/sign in](https://yingtongli.me/blog/2018/07/30/kobo-rego.html)
- [3: Gaining root telnet access](https://yingtongli.me/blog/2018/07/30/kobo-telnet.html)
- [4.1: Telnet over USB](https://yingtongli.me/blog/2018/07/30/kobo-telnet-usb.html)
- [4.2: Automatically starting telnet over USB on connect](https://yingtongli.me/blog/2018/07/30/kobo-autostart-usb.html)

Beware : the Wifi when activating Telnet is often disconnected, probably because of power saving.

## Installing Koreader and Plato with KFmon :

- [Koreader installation on Kobo devices](https://github.com/koreader/koreader/wiki/Installation-on-Kobo-devices)
- [Comment installer Koreader sur liseuse Kobo (avec KFMon / Kute File Monitor)](https://www.liseuses.info/224-comment-installer-koreader-liseuse-kobo-kfmon/)
- [One-Click Install Packages for KOReader & Plato](https://www.mobileread.com/forums/showthread.php?t=314220)

## SSH via Koreader :

- [SSH and SFTP via Koreader](https://github.com/koreader/koreader/wiki/SSH)

## KoboCloud :

- [KoboCloud](https://github.com/fsantini/KoboCloud)

## Kobo list of hacks :

- [Kobo Hacks and Utilities Index](https://www.mobileread.com/forums/showthread.php?t=295612)

## Night mode :

- [How to Get White Text with Black Background on Kobos – Night Mode](https://blog.the-ebook-reader.com/2018/04/22/how-to-get-white-text-with-black-background-on-kobos-night-mode/)
- [How to Get Night Mode (White Text & Black Background) on Kobo eReaders](https://the-digital-reader.com/2018/04/28/how-to-get-night-mode-white-text-black-background-on-kobo-ereaders/)
- [kobo-nightmode](https://dbeinder.github.io/kobo-nightmode/)

